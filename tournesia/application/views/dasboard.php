<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tournesia - Dashboard</title>
  <link rel="icon" href="<?php echo base_url(); ?>/assets/img/logo.png"/>
<!--
Holiday Template
http://www.templatemo.com/tm-475-holiday
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/flexslider.css" rel="stylesheet" >
  <link href="<?php echo base_url(); ?>/assets/css/templatemo-style.css" rel="stylesheet">
   <link href="<?php echo base_url(); ?>/assets/css/login.css" rel="stylesheet" >


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="tm-gray-bg">
  	<!-- Header -->
  	<div class="tm-header">
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-6 col-md-4 col-sm-3 tm-site-name-container">
  					<a href="#" class="tm-site-name"><img src="<?php echo base_url(); ?>/assets/img/icon.png" alt="Image" />TOURNESIA</a>	
  				</div>
	  			<div class="col-lg-6 col-md-8 col-sm-9">
	  				<div class="mobile-menu-icon">
		              <i class="fa fa-bars"></i>
		            </div>
	  				<nav class="tm-nav">
						<ul>
							
							<li><a href="<?php echo site_url('welcome/dasboard')?>"  class="active">Dashboard</a></li>
							<?php if($this->session->userdata('username')!="")
							{?>
							<li><a href="<?php echo site_url('welcome/dasboard')?>"><?php echo $this->session->userdata('username')?></a></li>
							<?php } ?>
							<li><a href="<?php echo site_url('Login/logout')?>">Logout</a></li>
						</ul>
					</nav>		
	  			</div>				
  			</div>
  		</div>	  	
  	</div>
	
	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
   
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
		<div class="login">
		  <h2 class="login-header">Log in</h2>

		  <form class="login-container">
			<p><input type="email" placeholder="Email"></p>
			<p><input type="password" placeholder="Password"></p>
			<p><input type="submit" value="Log in"></p>
		  </form>
		</div>
    </div>
  </div>
	
	<!-- Banner -->
	<section class="tm-banner">
		<!-- Flexslider -->
		<div class="flexslider flexslider-banner">
		  <ul class="slides">
		   		    <li>
			    <div class="tm-banner-inner">
					<h1 class="tm-banner-title">Jelajah <span class="tm-yellow-text">Indonesia</span></h1>
					<p class="tm-banner-subtitle">Berbagi Bersama</p>	
				</div>
				<img src="<?php echo base_url(); ?>/assets/img/rajaampat.jpg" alt="Image" />	
		    </li>
		    <li>
			    <div class="tm-banner-inner">
					<h1 class="tm-banner-title">Membangun <span class="tm-yellow-text">Indonesia</span> </h1>
					<p class="tm-banner-subtitle">Bersama Tournesia</p>
				</div>
		      <img src="<?php echo base_url(); ?>/assets/img/bromo.jpg" alt="Image" />
		    </li>
		  </ul>
		</div>		
	</section>

	<!-- gray bg -->	
	<section class="container tm-home-section-1" id="more">
		<div class="row">
			<!-- slider -->
			<div class="flexslider flexslider-about effect2">
			  <ul class="slides">
			    <li>
			      <img src="<?php echo base_url(); ?>/assets/img/komodo.png" alt="image" />
			      <div class="flex-caption">
			      	<h2 class="slider-title">Tournesia API</h2>
			      	<h3 class="slider-subtitle">Tournesia menyediakan REST API yang dapat digunakan untuk mengambil data wisata di seluruh Indonesia.</h3>
			      	<p class="slider-description">Data-data wisata dikumpulkan secara kolaboratif bersama-sama dengan menggunakan aplikasi android Tournesia. Validasi data
					dapat dilihat dengan menggunakan votes yang ada dalam fitur aplikasi.
					<br><br>
					Data tersebut akan diberikan kepada anda secara terbuka dalam JSON Format.</p>
			      </div>			      
			    </li>
			    <li>
			      <img src="<?php echo base_url(); ?>/assets/img/toba.png" alt="image" />
			      <div class="flex-caption">
			      	<h2 class="slider-title">Spesifikasi</h2>
			      	<h3 class="slider-subtitle">Ketentuan akses dan autentikasi API</h3>
			      	<p class="slider-description">Setelah mendaftar, setiap pengguna akan diberikan API Key unik sebagai password yang digunakan untuk memanggil API Tournesia.<br><br>
					Dalam pemanggilan API letakan api key pada URL seperti berikut : <br><br>
					<a href="hnwtvc.com/tournesia-rest/index.php/public/wisata/konten?api_key=YOUR_API_KEY">hnwtvc.com/tournesia-rest/index.php/public/wisata/konten?api_key=YOUR_API_KEY</a>
					</p>
			      </div>			      
			    </li>
			    <li>
			      <img src="<?php echo base_url(); ?>/assets/img/borobudur.png" alt="image" />
			      <div class="flex-caption">
			      	<h2 class="slider-title">Buat Aplikasi</h2>
			      	<h3 class="slider-subtitle">Bersama-sama memperluas wawasan wisata Indonesia.</h3>
			      	<p class="slider-description">Dengan mengembangkan aplikasi baru menggunakan API Tournesia. Anda telah membantu kami untuk meningkatkan nilai pariwisata Indonesia. <br><br>
			      </div>			      
			    </li>
			  </ul>
			</div>
		</div>
	
		<!--<div class="section-margin-top about-section">
			<div class="row">				
				<div class="tm-section-header">
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-6 col-md-6 col-sm-6"><h2 class="tm-section-title">Who We Are</h2></div>
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tm-about-box-1">
						<a href="#"><img src="<?php echo base_url(); ?>/assets/img/about-4.jpg" alt="img" class="tm-about-box-1-img"></a>
						<h3 class="tm-about-box-1-title">Thomas <span>( Founder )</span></h3>
						<p class="margin-bottom-15 gray-text">Proin gravida nibhvell aliquet. Aenean sollicitudin bibum auctor nisi elit.</p>
						<div class="gray-text">
							<a href="#" class="tm-social-icon"><i class="fa fa-twitter"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-facebook"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-pinterest"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-google-plus"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tm-about-box-1">
						<a href="#"><img src="<?php echo base_url(); ?>/assets/img/about-5.jpg" alt="img" class="tm-about-box-1-img"></a>
						<h3 class="tm-about-box-1-title">Keith <span>( Co-Founder )</span></h3>
						<p class="margin-bottom-15 gray-text">Proin gravida nibhvell aliquet. Aenean sollicitudin bibum auctor nisi elit.</p>
						<div class="gray-text">
							<a href="#" class="tm-social-icon"><i class="fa fa-twitter"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-facebook"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-pinterest"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-google-plus"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tm-about-box-1">
						<a href="#"><img src="<?php echo base_url(); ?>/assets/img/about-6.jpg" alt="img" class="tm-about-box-1-img"></a>
						<h3 class="tm-about-box-1-title">John <span>( General Manager )</span></h3>
						<p class="margin-bottom-15 gray-text">Proin gravida nibhvell aliquet. Aenean sollicitudin bibum auctor nisi elit.</p>
						<div class="gray-text">
							<a href="#" class="tm-social-icon"><i class="fa fa-twitter"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-facebook"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-pinterest"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-google-plus"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tm-about-box-1">
						<a href="#"><img src="<?php echo base_url(); ?>/assets/img/about-7.jpg" alt="img" class="tm-about-box-1-img"></a>
						<h3 class="tm-about-box-1-title">Smith <span>( Manager )</span></h3>
						<p class="margin-bottom-15 gray-text">Proin gravida nibhvell aliquet. Aenean sollicitudin bibum auctor nisi elit.</p>
						<div class="gray-text">
							<a href="#" class="tm-social-icon"><i class="fa fa-twitter"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-facebook"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-pinterest"></i></a>
							<a href="#" class="tm-social-icon"><i class="fa fa-google-plus"></i></a>
						</div>
					</div>
				</div>
			</div>		
		</div>-->
	</section>		
	
	<!-- white bg -->
	<section class="tm-white-bg section-padding-bottom">
		<div class="container">
			<div class="row">
				<div class="tm-section-header section-margin-top">
					<div class="col-lg-4 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-4 col-md-6 col-sm-6"><h2 class="tm-section-title">TOURNESIA API</h2></div>
					<div class="col-lg-4 col-md-3 col-sm-3"><hr></div>	
				</div>				
			</div>
			<div class="row">
				<!-- Testimonial -->
				<div class="col-lg-12">
					<div class="tm-what-we-do-right">
						<div class="tm-about-box-2 margin-bottom-30">
							
							<div class="tm-about-box-2-text">
								<h3 class="tm-about-box-2-title">API KEY</h3>
				                <p class="tm-about-box-2-description gray-text">Gunakan API KEY dalam setiap pemanggilan API Tournesia.
								<br> Nama Akun : <?php echo $this->session->userdata('username')?></p>
				                <p class="tm-about-box-2-footer"><?php echo $this->session->userdata('api_key')?></p>	
							</div>		                
						</div>
						<div class="tm-about-box-2">
							
							<div class="tm-about-box-2-text">
								<h3 class="tm-about-box-2-title">API Request</h3>
				                <p class="tm-about-box-2-description gray-text">
				                		Untuk mendapatkan seluruh data wisata yang ada :
				                		<br><br>
				                		hnwtvc.com/tournesia-rest/index.php/public/wisata/konten
								<br>
								?api_key=<?php echo $this->session->userdata('api_key')?><br><br>
								
								Untuk mendapatkan data wisata berdasarkan tag yang diberikan :
				                		<br><br>
				                		hnwtvc.com/tournesia-rest/index.php/public/wisata/konten
								<br>
								?tags=tags
								<br>
								?api_key=<?php echo $this->session->userdata('api_key')?><br><br>
								
								Untuk mendapatkan data wisata berdasarkan lokasi :
				                		<br><br>
				                		hnwtvc.com/tournesia-rest/index.php/public/wisata/konten
								<br>
								?lokasi=lokasi
								<br>
								?api_key=<?php echo $this->session->userdata('api_key')?><br><br>
								</p>
				                <p class="tm-about-box-2-footer">Jumlah Request : <?php echo $this->session->userdata('total_request')?></p>	
							</div>		                
						</div>
					</div>
					<div class="tm-testimonials-box">
						<h3 class="tm-testimonials-title">API Response</h3>
						<div class="tm-testimonials-content">
							<div class="tm-testimonial">
								<p>
					{<br>
					  "kontenData": [<br>
						{<br>
						  "Nama_Wisata": "Tugu Lampung",<br>
						  "Detail_Wisata": "Tempat berkumpul",<br>
						  "Gambar_Wisata": "",<br>
						  "Jumlah_Votes": "0",<br>
						  "Tags": "Tugu Lampung",<br>
						  "Lokasi": "Bandarlampung, Lampung"<br>
						},<br>
						{ ... }<br>
					  ],<br>
					  "status": 200,<br>
					  "message": "Berhasil"<br>
					}
              </p>
							</div>
              	
						</div>
					</div>	
				</div>							
			</div>			
		</div>
	</section>
	<footer class="tm-black-bg">
		<div class="container">
			<div class="row">
				<p class="tm-copyright-text">Copyright &copy; 2016 Tournesia 
                
                | Designed by PLBTW 2016</a></p>
			</div>
		</div>		
	</footer>
	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-1.11.2.min.js"></script>      		<!-- jQuery -->
  	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>					<!-- bootstrap js -->
  	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.flexslider-min.js"></script>			<!-- flexslider js -->
  	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/templatemo-script.js"></script>      		<!-- Templatemo Script -->
	<script>
		$(function() {

			// https://css-tricks.com/snippets/jquery/smooth-scrolling/
		  	$('a[href*=#]:not([href=#])').click(function() {
			    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			      var target = $(this.hash);
			      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			      if (target.length) {
			        $('html,body').animate({
			          scrollTop: target.offset().top
			        }, 1000);
			        return false;
			      }
			    }
		  	});		  	
		});
		$(window).load(function(){
			// Flexsliders
		  	$('.flexslider.flexslider-banner').flexslider({
			    controlNav: false
		    });
		  	$('.flexslider').flexslider({
		    	animation: "slide",
		    	directionNav: false,
		    	slideshow: false
		  	});
		});
	</script>
 </body>
 </html>